# The Birchmen's dotfiles

# Git Bare Repository
I manage my dotfiles with a git bare repository. There is a directory in my home directory called `dotfiles/` that contains all of the git files initiated by `git init --bare`. There are aliases in the `shell_aliases` file that allow me to interact wih my git bare repo with the command `gitdot`. The git repo is also configured with the `config --local status.showUntrackedFiles no` flag to prevent all of the files in your home directory showing as untracked files when interacting with the gti bare repo.

# Deploying My Dotfiles to a New Install
Clone the repo with:
`git clone --bare https://gitlab.com/the_birchmen/dotfiles.git $HOME/dotfiles`

Temporarily add the gitdot alias with:
`alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'`

Check out the content of the repo with:
`gitdot checkout`

An error message is likely to occur if there are existing config files from the fresh install. In this instance either delete the offending files or back them up with the following commands:
```
mkdir -p .config-backup && \
gitdot checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{}
```

# References
[Atlassian article](https://www.atlassian.com/git/tutorials/dotfiles)
[DistroTube video](https://youtu.be/tBoLDpTWVOM?si=iqCJi076qEZtHh8P)
[DistroTube's other video](https://youtu.be/r4CyUAFMUcY?si=FYmPJV6fpuqZKmMR)
