"       _
"__   _(_)_ __ ___  _ __ ___
"\ \ / / | '_ ` _ \| '__/ __|
" \ V /| | | | | | | | | (__
"(_)_/ |_|_| |_| |_|_|  \___|

"""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""

set hidden			" Hides unsaved buffers
set clipboard=unnamedplus	" copy paste between vim and everywhere
set splitbelow splitright

"{Backups and Undos}
set nobackup			" No Backup
set swapfile			" Swap
set undolevels=1000		" Number of undo levels
set backspace=indent,eol,start	" Backspace behaviour

"{Search}
set hlsearch			" Highlight all search results
set ignorecase			" Always case-insensitive
set incsearch			" Searches for strings incrementally
set wildmode=longest,list,full

"{Indents, Tabs and Spaces}
set autoindent			" Auto-indent new lines
set shiftwidth=4		" Number of auto-indent spaces
set softtabstop=4		" Number of spaces per Tab

"{Syntax and Line Settings}
syntax enable
set showmatch			" Highlight matching brace
set number relativenumber	" Show relative line numbers
set nowrap			" Dont wrap lines
set scrolloff=5			" Number of lines visible when scrolling

"{Status line}
set laststatus=2
set ruler			" Show row and column ruler information
set statusline=%f\ \ %y%m%r%h%w%=[%l,%v]\ \ \ \ \ \ [%L,%p%%]\ %n

"{Color and Appearance}
set t_Co=256                    " Set if term supports 256 colors.
set visualbell			" Use visual bell (no beeping)
colorscheme default

"""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Key Remapping
"""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Better tabbing
vnoremap < <gv
vnoremap > >gv

"" Tabs
nnoremap tn :tabnew<Space>
nnoremap tk :tabnext<CR>
nnoremap tj :tabprev<CR>
nnoremap th :tabfirst<CR>
nnoremap tl :tablast<CR>

" Remap splits navigation to just CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Other commands
map <leader>c :noh<CR>
