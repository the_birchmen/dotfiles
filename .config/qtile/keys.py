#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#        _   _ _
#   __ _| |_(_) | ___
#  / _` | __| | |/ _ \
# | (_| | |_| | |  __/
#  \__, |\__|_|_|\___|
#     |_|

import os
import subprocess
from typing import List

from libqtile import extension, layout, qtile
from libqtile.lazy import lazy
from libqtile.config import DropDown, Group, Key, Match, Rule, ScratchPad, Screen
from libqtile.dgroups import simple_key_binder

mod = "mod4"
myTerm = "alacritty"
backTerm = "xterm"
##### KEYBINDINGS #####

keys = [
    Key(
        [mod],
        "grave",
        lazy.group["scratchpad"].dropdown_toggle("term"),
        desc="Toggle Scratchpad",
    ),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle Layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Close Window"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Exits qtile session"),
    Key(
        [mod],
        "w",
        lazy.spawn("brave"),
        desc="Launches brave",
    ),
    Key(
        [mod, "shift"],
        "w",
        lazy.spawn("firefox"),
        desc="Launches firefox",
    ),
    Key(
        [mod, "control"],
        "w",
        lazy.spawn("librewolf"),
        desc="Launches librewolf",
    ),
    Key(
        [mod],
        "e",
        lazy.spawn(myTerm + " -e nvim"),
        desc="Launches neovim",
    ),
    Key(
        [mod],
        "r",
        lazy.spawn("dmenu_run -p 'Run: '"),
        desc="Dmenu run launcher",
    ),
    Key([mod, "shift"], "r", lazy.reload_config(), desc="Restarts Qtile"),
    Key(
        [mod],
        "t",
        lazy.spawn(myTerm + " -e btop"),
        desc="Launches bpytop",
    ),
    Key(
        [mod, "shift"],
        "t",
        lazy.spawn(myTerm + " -e htop"),
        desc="Launches htop",
    ),
    Key(
        [mod],
        "y",
        lazy.spawn("dmenu-edit-configs.sh"),
        desc="Dmenu edit config launcher",
    ),
    Key(
        [mod],
        "u",
        lazy.run_extension(extension.J4DmenuDesktop(dmenu_command="dmenu -c -l 15")),
        desc="Dmenu J4 Desktop launcher",
    ),
    Key(
        [mod, "shift"],
        "u",
        lazy.spawn("dmenu -show run"),
        desc="Dmenu run launcher centered",
    ),
    Key(
        [mod],
        "i",
        lazy.spawn("dmenu-internet.sh"),
        desc="Dmenu internet launcher",
    ),
    Key(
        [mod],
        "o",
        lazy.spawn("dmenu-office.sh"),
        desc="Dmenu office apps launcher",
    ),
    Key(
        [mod],
        "p",
        lazy.spawn("dmenu-power.sh"),
        desc="Dmenu power settings launcher",
    ),
    Key(
        [mod],
        "a",
        lazy.spawn(myTerm + " -e pulsemixer"),
        desc="Launches pulesmixer",
    ),
    Key(
        [mod],
        "s",
        lazy.spawn("dmenu-manage.sh"),
        desc="Dmenu control panel launcher",
    ),
    Key(
        [mod, "shift"],
        "d",
        lazy.spawn("emacsclient -c -a 'emacs'"),
        desc="Launches Doom Emacs",
    ),
    Key(
        [mod],
        "d",
        lazy.spawn("emacs"),
        desc="Launches Doom Emacs",
    ),
    Key(
        [mod],
        "f",
        lazy.spawn(myTerm + " -e vifm"),
        desc="Launches CLI file manager (VIFM)",
    ),
    Key(
        [mod, "shift"],
        "f",
        lazy.spawn("caja"),
        desc="Launches GUI filemanager (Caja)",
    ),
    Key(
        [mod],
        "g",
        lazy.spawn("dmenu-games.sh"),
        desc="Dmenu games launcher",
    ),
    Key(
        [mod, "shift"],
        "g",
        lazy.spawn(myTerm + " -e amfora"),
        desc="Launched Amfora",
    ),
    Key(
        [mod, "shift"],
        "h",
        lazy.layout.shrink(),
        desc="Shrink size of current window (XmonadTall)",
    ),
    Key(
        [mod],
        "j",
        lazy.layout.down(),
        desc="Switch between windows in the current stack pane",
    ),
    Key(
        [mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        desc="Move windows down in current stack",
    ),
    Key(
        [mod],
        "k",
        lazy.layout.up(),
        desc="Switch between windows in current stack pane",
    ),
    Key(
        [mod, "shift"],
        "k",
        lazy.layout.shuffle_up(),
        desc="Move windows up or down in current stack",
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.grow(),
        desc="Grow size of current window (XmonadTall)",
    ),
    Key(
        [mod],
        "Return",
        lazy.spawn(myTerm),
        desc="Launches my terminal",
    ),
    Key(
        [mod, "shift"],
        "Return",
        lazy.spawn(backTerm),
        desc="Launches backup terminal",
    ),
    Key([mod], "x", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key(
        [mod],
        "c",
        lazy.spawn(myTerm + " -e nvim  /home/dustin/.config/qtile/config.py"),
        desc="Launches my config in nvim",
    ),
    Key(
        [mod, "shift"],
        "c",
        lazy.spawn(myTerm + " -e cmatrix"),
        desc="Launches cmatrix",
    ),
    Key(
        [mod, "shift"],
        "v",
        lazy.spawn("virtualbox"),
        desc="Launches virtualbox",
    ),
    Key(
        [mod],
        "v",
        lazy.spawn("virt-manager"),
        desc="Launches VirtManager",
    ),
    Key(
        [mod],
        "b",
        lazy.run_extension(extension.WindowList(item_format="{group}: {window}")),
        desc="Open Window launcher",
    ),
    Key(
        [mod],
        "n",
        lazy.spawn(myTerm + " -e newsboat"),
        desc="Launches Newsboat",
    ),
    Key(
        [mod, "shift"],
        "n",
        lazy.spawn(myTerm + " -e castero"),
        desc="Launches castero",
    ),
    Key(
        [mod],
        "m",
        lazy.spawn(myTerm + " -e mocp"),
        desc="Launches MOC",
    ),
    Key([mod], "comma", lazy.to_screen(0), desc="Keyboard focus to monitor 1"),
    Key([mod], "period", lazy.to_screen(1), desc="Keyboard focus to monitor 2"),
    Key([mod, "shift"], "space", lazy.window.toggle_floating(), desc="Toggle Floating"),
    Key(
        [],
        "XF86Mail",
        lazy.spawn("brave https://mail.protonmail.com/login"),
        desc="Go To Proton Mail",
    ),
    Key([], "XF86AudioPrev", lazy.spawn("mocp -r"), desc="Volume Mute"),
    Key([], "XF86AudioNext", lazy.spawn("mocp -f"), desc="Volume Mute"),
    Key([], "XF86AudioPlay", lazy.spawn("mocp -G"), desc="Volume Mute"),
    Key([], "XF86AudioStop", lazy.spawn("mocp -x"), desc="Volume Mute"),
    Key([], "XF86AudioMute", lazy.spawn("pamixer -t"), desc="Volume Mute"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 5"), desc="Volume Up"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer  -d 5"), desc="Volume Down"),
    Key(
        [], "XF86Calculator", lazy.spawn("mate-calculator"), desc="Launches Calculator"
    ),
    Key(
        [mod],
        "Print",
        lazy.spawn("scrot -e 'mv $f ~/Pictures/scrots/'"),
        desc="Take a Screenshot",
    ),
]
