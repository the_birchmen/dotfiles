#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#        _   _ _
#   __ _| |_(_) | ___
#  / _` | __| | |/ _ \
# | (_| | |_| | |  __/
#  \__, |\__|_|_|\___|
#     |_|

import os
import subprocess
from typing import List

from keys import keys
from libqtile import bar, extension, hook, layout, qtile, widget
from libqtile.lazy import lazy
from libqtile.config import (
    Click,
    Drag,
    DropDown,
    Group,
    Key,
    Match,
    Rule,
    ScratchPad,
    Screen,
)
from libqtile.dgroups import simple_key_binder
from qtile_extras import widget
from qtile_extras.widget.decorations import PowerLineDecoration
from themes import colors

##### DEFENITIONS ######

mod = "mod4"
myTerm = "alacritty"
backTerm = "xterm"
dropTerm = "st"
myFont = "Hack"


##### GROUPS #####
groups = (
    Group("", layout="max"),
    Group("", layout="monadtall"),
    Group("", layout="monadtall"),
    Group("", layout="max"),
    Group("", layout="max"),
    Group("", layout="monadtall"),
    Group("", layout="monadwide", matches=[Match(wm_class=["corectrl"])]),
    Group("", layout="floating", matches=[Match(wm_class=["Steam"])]),
    Group("", layout="max"),
    ScratchPad(
        "scratchpad",
        [DropDown("term", dropTerm, height=0.9, opacity=1.0, on_focus_lost_hide=False)],
    ),
)

for i, group in enumerate(groups[:-1], 1):
    # Switch to another group
    keys.append(Key([mod], str(i), lazy.group[group.name].toscreen()))
    # Send current window to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(group.name)))
    # Send current window to another group and go to that group
    keys.append(
        Key(
            [mod, "control"], str(i), lazy.window.togroup(group.name, switch_group=True)
        )
    )

##### LAYOUTS #####

layout_defaults = {
    "border_width": 3,
    "margin": 6,
    "border_focus": colors[2],
    "border_normal": colors[4],
}


layouts = [
    layout.Max(),
    layout.MonadTall(ratio=0.50, **layout_defaults),
    layout.Floating(**layout_defaults),
    layout.MonadWide(ratio=0.35, **layout_defaults),
    # layout.Matrix(**layout_defaults),
    # layout.Stack(num_stacks=2, **layout_defaults),
    # layout.TreeTab(
    #     	font = "Mononoki Nerd Font Mono",
    #             fontsize = 14,
    #             sections = ["Tabs"],
    #             section_fontsize = 18,
    #             bg_color = colors[0],
    #             active_bg = colors[2],
    #             active_fg = colors[0],
    #             inactive_bg = colors[0],
    #             inactive_fg = colors[4],
    #             padding_y = 5,
    #             section_top = 10,
    #             panel_width = 120,
    #     	**layout_defaults),
    # layout.Bsp(),
    # layout.Columns(),
    # layout.RatioTile(),
    # layout.Spiral(),
    # layout.Tile(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

##### WIDGETS & Panel #####

widget_defaults = dict(
    font=myFont,
    fontsize=16,
    padding=5,
    linewidth=1,
    background=colors[0],
)

powerline = {"decorations": [PowerLineDecoration(path="back_slash")]}


def init_widgets_list():
    widgets_list = [
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            background=colors[4],
        ),
        widget.Sep(
            size_percent=100,
            foreground=colors[0],
        ),
        widget.GroupBox(
            # padding_x = 7,
            padding_y=0,
            margin_y=3,
            margin_x=0,
            active=colors[4],
            inactive=colors[7],
            this_current_screen_border=colors[4],
            this_screen_border=colors[4],
            other_current_screen_border=colors[7],
            other_screen_border=colors[7],
            # block_highlight_text_color=colors[0],
            rounded=False,
            highlight_method="line",
            highlight_color=colors[0],
        ),
        widget.Prompt(
            padding=10,
            foreground=colors[4],
        ),
        widget.TaskList(
            foreground=colors[7],
            border=colors[4],
            # unfocused_border = colors[7],
            urgent_border=colors[1],
            title_width_method="uniform",
            max_title_width=120,
            highlight_method="border",
            rounded=True,
            padding_y=2,
            # padding_y = 0,
            # margin_y = -1,
            spacing=6,
            icon_size=0,
            **powerline,
        ),
        widget.Systray(
            background=colors[4],
            icon_size=24,
            padding=5,
            **powerline,
        ),
        widget.Moc(
            fontsize=18,
            max_chars=30,
            play_color=colors[0],
            noplay_color=colors[4],
            background=colors[7],
            **powerline,
        ),
        widget.CheckUpdates(
            execute="xterm -hold -e sudo pacman -Syu",
            distro="Arch_checkupdates",
            colour_have_updates=colors[0],
            background=colors[2],
            # no_update_string = ' 0',
            update_interval=860,
            **powerline,
        ),
        widget.Net(
            background=colors[7],
            foreground=colors[0],
            format="{down:.0f}{down_suffix:<2} ↓↑ {up:.0f}{up_suffix:<2}",
            **powerline,
        ),
        widget.DF(
            format="~/{uf}{m}|{r:.0f}%",
            partition="/home",
            background=colors[4],
            foreground=colors[0],
            visible_on_warn=False,
            **powerline,
        ),
        widget.Memory(
            background=colors[7],
            foreground=colors[0],
            format="{MemUsed:.1f}{mm}",
            measure_mem="G",
            mouse_callbacks={"Button1": lazy.spawn(myTerm + " -e htop")},
            **powerline,
        ),
        widget.Clock(
            foreground=colors[0],
            background=colors[4],
            format=" %d%b%Y %a %I:%M %p",
            mouse_callbacks={
                "Button1": lazy.spawn("xterm -hold -e curl wttr.in/Canaan")
            },
        ),
    ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    del widgets_screen1[5:7]  # Slicing removes unwanted widgets (systray) on Monitor 1
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[8:10]  # Slicing removes unwanted widgets (systray) on Monitor 2
    return widgets_screen2  # Monitor 2 will display all widgets in widgets_list


def init_screens():
    return [
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=28)),
        Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=28)),
    ]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()


# Drag floating layouts.

mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

border_focus = colors[4]
border_normal = colors[0]
dgroups_key_binder = None
dgroups_app_rules: List = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_minimize = True

floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="mate-calculator"),
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)

auto_fullscreen = True
focus_on_window_activation = "smart"

##### STARTUP APPLICATIONS #####
search_paths = [
    # '/etc/xdg/autostart',
    # os.path.expanduser('~/.config/autostart'),
    os.path.expanduser('~/.config/qtile/autostart'),
]

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    autostart_paths = ':'.join(search_paths)
    subprocess.run(['/usr/bin/dex', '-as', autostart_paths])
    subprocess.call([home + "/.config/qtile/autostart.sh"])

wmname = "qtile"
