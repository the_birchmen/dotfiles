# Spacedust
Spacedust = [
    ["#171010"],  # black 0
    ["#d51a29"],  # red 1
    ["#98971a"],  # green 2
    ["#fabd2f"],  # yellow 3
    ["#016de2"],  # blue 4
    ["#6f4ffd"],  # purple 5
    ["#2aa198"],  # cyan 6
    ["#d4d4d4"],
]  # white 7

# Doom one
DoomOne = [
    ["#1c1f24"],  # black 0
    ["#ff6c6b"],  # red 1
    ["#98be65"],  # green 2
    ["#da8548"],  # yellow 3
    ["#51afef"],  # blue 4
    ["#c678dd"],  # purple 5
    ["#5699af"],  # cyan 6
    ["#dfdfdf"],
]  # white 7

# Gruvbox Dark
GruvboxDark = [
    ["#282828"],  # black 0
    ["#cc241d"],  # red 1
    ["#98971a"],  # green 2
    ["#d79921"],  # yellow 3
    ["#458588"],  # blue 4
    ["#b16286"],  # purple 5
    ["#689d6a"],  # cyan 6
    ["#ebdbb2"],
]  # white 7
# Nord
Nord = [
    ["#3B4252"],  # black 0
    ["#BF616A"],  # red 1
    ["#A3BE8C"],  # green 2
    ["#EBCB8B"],  # yellow 3
    ["#81A1C1"],  # blue 4
    ["#B48EAD"],  # purple 5
    ["#88C0D0"],  # cyan 6
    ["#E5E9F0"],
]  # white 7

# Dracula
Dracula = [
    ["#000000"],  # black 0
    ["#ff5555"],  # red 1
    ["#50fa7b"],  # green 2
    ["#f1fa8c"],  # yellow
    ["#bd93f9"],  # blue 4
    ["#ff79c6"],  # purple
    ["#8be9fd"],  # cyan 6
    ["#bfbfbf"],
]  # white 7

# Tokyo Night
TokyoNight = [
    ["#32344a"],
    ["#f7768e"],
    ["#9ece6a"],
    ["#e0af68"],
    ["#7aa2f7"],
    ["#ad8ee6"],
    ["#449dab"],
    ["#787c99"],
]


colors = TokyoNight
