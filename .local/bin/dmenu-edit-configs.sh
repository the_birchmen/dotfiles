#!/usr/bin/env bash
#
#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#
# Description: Choose from a list of configuration files to edit. Forked from DT's scripts repo.
# Dependencies: dmenu
# GitLab: https://www.gitlab.com/dwt1/dmscripts
# License: https://www.gitlab.com/dwt1/dmscripts/LICENSE
# Contributors: Derek Taylor, the_birchmen


# Defining the text editor to use.
# EDITOR="st -e vim"
# EDITOR="st -e nvim"
EDITOR="emacsclient -c -a emacs"

# An array of options to choose.
# You can edit this list to add/remove config files.
declare -a options=(
"alacritty - $HOME/.config/alacritty/alacritty.yml"
"bash - $HOME/.bashrc"
"dunst - $HOME/.config/dunst/dunstrc"
"dmenu - $HOME/suckless/dmenu-birch/config.def.h"
"dwm - $HOME/suckless/dwm-birch/config.def.h"
"dwmblocks - $HOME/suckless/dwmblocks-birch/blocks.def.h"
"i3 - $HOME/.config/i3/config"
"moc - $HOME/.moc/config"
"neovim - $HOME/.config/nvim/init.vim"
"picom - $HOME/.config/picom/picom.conf"
"qtile - $HOME/.config/qtile/config.py"
"quickmarks - $HOME/.config/qutebrowser/quickmarks"
"qutebrowser - $HOME/.config/qutebrowser/autoconfig.yml"
"ranger - $HOME/.config/ranger/rc.conf"
"shell aliases - $HOME/.config/shell_aliases"
"st - $HOME/suckless/st-birch/config.def.h"
"vifm - $HOME/.config/vifm/vifmrc"
"vim - $HOME/.vimrc"
"xresources - $HOME/.Xresources"
"zsh - $HOME/.zshrc"
"quit"
)

# Piping the above array into dmenu.
# We use "printf '%s\n'" to format the array one item to a line.
choice=$(printf '%s\n' "${options[@]}" | dmenu -l 20 -p 'Edit config:')

# What to do when/if we choose 'quit'.
if [[ "$choice" == "quit" ]]; then
    echo "Program terminated." && exit 0

# What to do when/if we choose a file to edit.
elif [ "$choice" ]; then
	cfg=$(printf '%s\n' "${choice}" | awk '{print $NF}')
	$EDITOR "$cfg"

# What to do if we just escape without choosing anything.
else
    echo "Program terminated." && exit 0
fi
