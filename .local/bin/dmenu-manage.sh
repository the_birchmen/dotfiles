#!/bin/bash
#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#
# Dmenu script for launching managment apps.


declare -a options=("htop
Manjaro-Settings
pamac
psensor
lxappearance
quit")

choice=$(echo -e "${options[@]}" | dmenu -c -l 15)

case $choice in
	quit)
		echo "Program terminated." && exit 1
	;;
	htop)
		choice="st -e htop"
	;;
	pamac)
		choice="pamac-manager"
	;;
	Manjaro-Settings)
		choice="manjaro-settings-manager"
	;;
	psensor| \
	lxappearance)
	;;
esac

exec $choice
