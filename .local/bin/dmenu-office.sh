#!/bin/bash
#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#
# Dmenu script for launching office programs.


declare -a options=("LibreOffice
neovim
markdown
simplenote
wordgrinder
nano
quit")

choice=$(echo -e "${options[@]}" | dmenu -c -l 15)

case $choice in
	quit)
		echo "Program terminated." && exit 1
	;;
	LibreOffice)
		choice="libreoffice"
	;;
	neovim)
		choice="st nvim"
	;;
	markdown)
		choice="retext"
	;;
	simplenote)
		choice="Simplenote-linux-1.16.0-x86_64.AppImage"
	;;
	wordgrinder)
		choice="st wordgrinder"
	;;
	nano)
		choice="st nano"
	;;
esac

exec $choice
