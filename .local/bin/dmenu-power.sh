#!/bin/bash

# Simple script to handle a DIY shutdown menu. When run you should see a bunch of options (shutdown, reboot etc.)
#
# Requirements:
# - dmenu
# - systemd, but you can replace the commands for OpenRC or anything else
#
# Instructions:
# - Save this file as power.sh or anything
# - Give it exec priviledge, or chmod +x /path/to/power.sh
# - Run it

declare options=("Shutdown
Lock
Reboot
Suspend
Cancel")


choice=$(echo -e "${options[@]}" | dmenu -c -l 15)

# https://www.freedesktop.org/software/systemd/man/systemd-sleep.conf.html#Description

case "$choice" in
	Cancel)
		echo "Program terminated." && exit 1
	;;
	Lock)
	    dm-tool lock
	;;
	Shutdown)
		systemctl poweroff
	;;
	Reboot)
		systemctl reboot
	;;
	Suspend)
		systemctl suspend
	;;
	*)
		exit 1
	;;
esac
st "$choice"
