#!/bin/bash
#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#
# Dmenu script for launching games.

    declare -a options=("steam
0ad
xonotic
dosbox
lutris
morrowind
psensor
retroarch
supertuxkart
quit")

choice=$(echo -e "${options[@]}" | dmenu -c -l 15)

case $choice in
	quit)
		echo "Program terminated." && exit 1
	;;
	0ad)
		choice="0ad.sh"
	;;
	xonotic)
		choice="xonotic.sh"
	;;
	morrowind)
		choice="morrowind.sh"
	;;
	;;
	steam| \
	psensor| \
	dosbox| \
	lutris|\
	retroarch| \
	supertuxkart)
	;;
esac

exec $choice
