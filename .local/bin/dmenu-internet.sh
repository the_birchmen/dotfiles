#!/bin/bash
#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#
# Dmenu script for launching internet programs.


declare -a options=("firefox
castero
lbry
lynx
qutebrowser
reddit
transmission
quit")

choice=$(echo -e "${options[@]}" | dmenu -c -l 15)

case $choice in
	quit)
		echo "Program terminated." && exit 1
	;;
	castero)
		choice="st -e castero"
	;;
	lbry)
		choice="LBRY_0.47.2.AppImage"
	;;
	lynx)
		choice="st -e lynx"
	;;
	reddit)
		choice="st -e tuir"
	;;
	transmission)
		choice="transmission-gtk"
	;;
	firefox| \
	qutebrowser)
	;;
esac

exec $choice
