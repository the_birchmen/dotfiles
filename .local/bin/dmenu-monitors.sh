#!/bin/bash
#  _   _              _     _          _
# | |_| |__   ___    | |__ (_)_ __ ___| |__  _ __ ___   ___ _ __
# | __| '_ \ / _ \   | '_ \| | '__/ __| '_ \| '_ ` _ \ / _ \ '_ \
# | |_| | | |  __/   | |_) | | | | (__| | | | | | | | |  __/ | | |
#  \__|_| |_|\___|___|_.__/|_|_|  \___|_| |_|_| |_| |_|\___|_| |_|
#               |_____|
#
# Dmenu script for changing monitor settings.


declare -a options=("single
dual
quit")

choice=$(echo -e "${options[@]}" | dmenu)

case $choice in
	quit)
		echo "Program terminated." && exit 1
	;;
	single)
		choice="sh single.sh"
	;;
	dual)
		choice="sh dual.sh"
	;;
esac

exec $choice
